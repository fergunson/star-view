package com.ufla.fsouza.starview.Models.moon;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by fsouza on 02/05/17.
 * //pega os dados JSON da Lua na API burningsoul no seguinte formato:
     *   age	7.3308883945571
         illumination	49.449378731572
         stage	"waxing"
         DFCOE	373905.57
         DFS	150826318.98
         FM{
             UT	1494452636.29
             DT	"21:43:56-10 May 2017"
            }
         NNM{
             UT	1495741582.3828
             DT	"19:46:22-25 May 2017"
            }

    Classe MoonController herda da classe Json e trata o JSON cru obtido da API
    ele nos fornecerá diversos dados da Lua já processados
 */

/**
 * JSON FARM SENSE
 0
 Error : 0
 ErrorMsg : "success"
 TargetDate : "1350526582"
 Moon
 Index : 2
 Age : 2.9555953469264
 Phase : "Waxing Crescent"
 Distance : 363325.22
 Illumination : 0.1
 AngularDiameter : 0.54815395361483
 DistanceToSun : 149016616.79983
 SunAngularDiameter : 0.53520976935835
 */

public class Moon {

    @SerializedName("Error")
    private int error;

    @SerializedName("ErrorMsg")
    private String errorMsg;

    @SerializedName("Moon")
    private ArrayList<String> moonStages;

    @SerializedName("Index")
    private int index;

    @SerializedName("Age")
    private double age;

    @SerializedName("Phase")
    private String phase;

    @SerializedName("Distance")
    private double distance;

    @SerializedName("Illumination")
    private double illumination;

    @SerializedName("AngularDiameter")
    private double angularDiameter;

    @SerializedName("DistanceToSun")
    private double distanceToSun;

    @SerializedName("SunAngularDiameter")
    private double sunAngularDiameter;

    public Moon(int error, String errorMsg, ArrayList<String> moonStages, int index, double age, String phase, double distance, double illumination, double angularDiameter, double distanceToSun, double sunAngularDiameter) {
        this.error = error;
        this.errorMsg = errorMsg;
        this.moonStages = moonStages;
        this.index = index;
        this.age = age;
        this.phase = phase;
        this.distance = distance;
        this.illumination = illumination;
        this.angularDiameter = angularDiameter;
        this.distanceToSun = distanceToSun;
        this.sunAngularDiameter = sunAngularDiameter;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ArrayList<String> getMoonStages() {
        return moonStages;
    }

    public void setMoonStages(ArrayList<String> moonStages) {
        this.moonStages = moonStages;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public double getAge() {
        return age;
    }

    public void setAge(double age) {
        this.age = age;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getIllumination() {
        return illumination*100;
    }

    public void setIllumination(double illumination) {
        this.illumination = illumination;
    }

    public double getAngularDiameter() {
        return angularDiameter;
    }

    public void setAngularDiameter(double angularDiameter) {
        this.angularDiameter = angularDiameter;
    }

    public double getDistanceToSun() {
        return distanceToSun;
    }

    public void setDistanceToSun(double distanceToSun) {
        this.distanceToSun = distanceToSun;
    }

    public double getSunAngularDiameter() {
        return sunAngularDiameter;
    }

    public void setSunAngularDiameter(double sunAngularDiameter) {
        this.sunAngularDiameter = sunAngularDiameter;
    }

    @Override
    public String toString() {
        return "Iluminação Lua "+ String.valueOf(illumination*100);
    }
}
