package com.ufla.fsouza.starview.Models.moon;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by fsouza on 02/05/17.
 * //pega os dados JSON da Lua na API burningsoul no seguinte formato:
 *   age	7.3308883945571
 illumination	49.449378731572
 stage	"waxing"
 DFCOE	373905.57
 DFS	150826318.98
 FM{
 UT	1494452636.29
 DT	"21:43:56-10 May 2017"
 }
 NNM{
 UT	1495741582.3828
 DT	"19:46:22-25 May 2017"
 }

 Classe MoonController herda da classe Json e trata o JSON cru obtido da API
 ele nos fornecerá diversos dados da Lua já processados
 */

/**
 * JSON FARM SENSE
 0
 Error : 0
 ErrorMsg : "success"
 TargetDate : "1350526582"
 Moon
 Index : 2
 Age : 2.9555953469264
 Phase : "Waxing Crescent"
 Distance : 363325.22
 Illumination : 0.1
 AngularDiameter : 0.54815395361483
 DistanceToSun : 149016616.79983
 SunAngularDiameter : 0.53520976935835
 */

public class MoonList {

    @SerializedName("0")
    private ArrayList<Moon> moonArrayList;

    public MoonList(ArrayList<Moon> moonArrayList) {
        this.moonArrayList = moonArrayList;
    }

    public ArrayList<Moon> getMoonArrayList() {
        return moonArrayList;
    }

    public void setMoonArrayList(ArrayList<Moon> moonArrayList) {
        this.moonArrayList = moonArrayList;
    }
}