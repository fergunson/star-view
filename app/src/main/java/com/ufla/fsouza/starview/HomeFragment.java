package com.ufla.fsouza.starview;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.ufla.fsouza.starview.Adapters.CondicaoDiaAdapter;
import com.ufla.fsouza.starview.Controllers.MasterController;
import com.ufla.fsouza.starview.Models.CondicaoDia;
import com.ufla.fsouza.starview.R;

import java.io.Serializable;
import java.util.ArrayList;


public class HomeFragment extends Fragment implements Serializable{
    final public String nomeFragmento = "Home";
    private ConstraintLayout mLayoutDados;
    private LinearLayout mProgressBar;
    private SwipeRefreshLayout mLayoutTodo;
    private TextView porcentagem, lightPollution, humidity, clouds,
            quality, moonIllumination, temperature, wind,
            location, dataTime, melhorData, melhorPorcentagem, melhorAvaliacao;
    private ListView lista;
    private MasterController masterController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        porcentagem = (TextView) view.findViewById(R.id.porcentagem);
        lightPollution = (TextView) view.findViewById(R.id.light_pollution);
        humidity = (TextView) view.findViewById(R.id.humidity);
        clouds = (TextView) view.findViewById(R.id.clouds);
        quality = (TextView) view.findViewById(R.id.view_quality);
        moonIllumination = (TextView) view.findViewById(R.id.moon_illumination);
        temperature = (TextView) view.findViewById(R.id.temperature);
        wind =(TextView) view.findViewById(R.id.wind);
        mProgressBar = (LinearLayout) view.findViewById(R.id.progressBar6);
        mLayoutDados =(ConstraintLayout) view.findViewById(R.id.dados);
        location = (TextView) view.findViewById(R.id.location_text);
        dataTime = (TextView) view.findViewById(R.id.current_data_time);
        melhorData = (TextView) view.findViewById(R.id.data_melhor);
        melhorAvaliacao = (TextView) view.findViewById(R.id.avaliacao_melhor);
        melhorPorcentagem = (TextView) view.findViewById(R.id.porcentagem_melhor);

        mLayoutTodo = (SwipeRefreshLayout) view.findViewById(R.id.refresh_home);
        iniciaCarregamento();

        masterController.getHomeController().atualizaDados();

        mLayoutTodo.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mLayoutTodo.setRefreshing(true);
                Log.d("refresh","true");
                masterController.getHomeController().atualizaDados();
            }
        });
        return view;
    }

    public void erroConexao(){
        try{
            Toast.makeText(getContext(),getString(R.string.data_error),Toast.LENGTH_LONG).show();
        }catch (SecurityException e) {
        Log.d("Error: ","Não conseguiu postar toast de conexão");
    }

    }

    public void iniciaCarregamento(){
        mLayoutDados.setVisibility(View.INVISIBLE);
        mLayoutTodo.setAlpha((float)0.5);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    public void mostraDados(){
        mLayoutDados.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mLayoutTodo.setAlpha((float)0.9);
    }

    public void escreveDados(ArrayList<CondicaoDia> condicaoDias,CondicaoDia melhorCondicao,String nomeLugar){
        porcentagem.setText(String.valueOf(condicaoDias.get(0).getPorcentagem()));
        quality.setText(getString(R.string.conditions)+getString(condicaoDias.get(0).avalia()));
        lightPollution.setText(getString(R.string.light_pollution) +
                String.valueOf(condicaoDias.get(0).getRadiance())+"%");
        humidity.setText(getString(R.string.humidity)+" "+
                String.valueOf(condicaoDias.get(0).getForecast().getPrevisaoPrincipal().getHumidity())+"%");
        clouds.setText(getString(R.string.clouds)+" " +
                String.valueOf((int)condicaoDias.get(0).getForecast().getClouds().getCloudsPercent())+"%");
        moonIllumination.setText(getString(R.string.moon_illumination)+" "+
                String.valueOf(condicaoDias.get(0).getMoonIllumination())+"%");
        temperature.setText(getString(R.string.temperature)+" ⇧"
                +condicaoDias.get(0).getForecast().getPrevisaoPrincipal().getTempMax()+getString(R.string.celcius)+"   ⇩"+
                condicaoDias.get(0).getForecast().getPrevisaoPrincipal().getTempMin()+getString(R.string.celcius));
        wind.setText(getString(R.string.wind)+"  "+getString(R.string.speed) +condicaoDias.get(0).getForecast().getWind().getSpeed()+" m/s   " +
                getString(R.string.direction) +condicaoDias.get(0).getForecast().getWind().getDirection()+"º");

        //data.setText(context.getString(condicaoDia.getForecast().diaDaSemanaParaR()) + " " + condicaoDia.getForecast().getDataCalculada() + " - " + condicaoDia.getForecast().getHora()+"h");

        melhorData.setText(getString(melhorCondicao.getForecast().diaDaSemanaParaR())+" "+melhorCondicao.getForecast().getDataCalculada()+" "+melhorCondicao.getForecast().getHora()+"h");
        melhorPorcentagem.setText(String.valueOf(melhorCondicao.getPorcentagem())+"%");
        melhorAvaliacao.setText(getString(melhorCondicao.avalia()));

        dataTime.setText(condicaoDias.get(0).getForecast().getDate().substring(0,16));

        location.setText(nomeLugar);

        lista = (ListView) getView().findViewById(R.id.next_hours);
        CondicaoDiaAdapter condicaoDiaAdapter = new CondicaoDiaAdapter(condicaoDias,getContext());

        lista.setAdapter(condicaoDiaAdapter);
        mLayoutTodo.setRefreshing(false);
        mostraDados();
    }

    @Override
    public void onDestroyView() {
        masterController.getHome().getAboutFragment().setMasterController(masterController);
        masterController.getHome().getGoogleMapsFragment().setMasterController(masterController);
        super.onDestroyView();
        Log.d("Destruiu view home ", "destrui");
    }

    @Override
    public void onDestroy() {
        masterController.getHome().getAboutFragment().setMasterController(masterController);
        masterController.getHome().getGoogleMapsFragment().setMasterController(masterController);
        super.onDestroy();
        Log.d("Fragment", "Destrui Home");
    }

    @Override
    public String toString() {
        return nomeFragmento;
    }

    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }

}
