package com.ufla.fsouza.starview.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fsouza on 24/07/17.
 */

public class ApiLightPollution {
    private static final String ENDPOINT = "https://www.lightpollutionmap.info/";

    public Retrofit getLightPolutionRetrofit(){
        // Trailing slash is needed

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
