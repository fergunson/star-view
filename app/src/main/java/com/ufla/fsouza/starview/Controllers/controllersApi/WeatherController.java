package com.ufla.fsouza.starview.Controllers.controllersApi;

import android.util.Log;

import com.ufla.fsouza.starview.Api.ApiServiceWeather;
import com.ufla.fsouza.starview.Api.ApiWeather;
import com.ufla.fsouza.starview.Controllers.HomeController;
import com.ufla.fsouza.starview.Models.previsao.Cidade;
import com.ufla.fsouza.starview.Models.previsao.Previsao;
import com.ufla.fsouza.starview.Models.previsao.WeatherList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class WeatherController {
    private Retrofit retrofit;
    private final ApiServiceWeather apiService;
    private ArrayList<Previsao> previsoes;
    private Cidade cidade;
    private static final String weatherApiKey="c0007f32c492501c967fb2f17f157e05";
    private HomeController controle;

    public WeatherController(HomeController controle){
        ApiWeather api = new ApiWeather();
        retrofit = api.getWeatherRetrofit();
        apiService = retrofit.create(ApiServiceWeather.class);
        this.controle=controle;
    }

    public void refreshForecast(String lon,
                                String lat,
                                String units,
                                String lang) {
        final boolean[] sucess = {false};
        previsoes = new ArrayList<>();
        Call call = apiService.getWeather(lon, lat, units, lang, weatherApiKey);
        Log.d("URL WEATHER", call.request().url().toString());
        call.enqueue(new Callback<WeatherList>() {
            @Override
            public void onResponse(Call<WeatherList> call, Response<WeatherList> response) {
                if (response.isSuccessful()) {

                    previsoes = response.body().getPrevisoes();
                    controle.setGotWeatherData(1);
                    controle.confereSeChegouDados();
                    cidade=response.body().getCidade();
                    Log.d("WeatherList", response.body().getPrevisoes().toString());

                } else {
                    //diz que não foi possível obter previsões
                    controle.setGotWeatherData(2);
                    controle.confereSeChegouDados();
                    Log.d("ERRO", "não foi possível obter dados");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("ERRO", "On Failure WeatherList");
                Log.d("ERRO", t.getMessage()+"");
                controle.setGotWeatherData(2);
                controle.confereSeChegouDados();
            }
        });
    }

    public ArrayList<Previsao> getPrevisoes() {
        return previsoes;
    }

    public Cidade getCidade() {
        return cidade;
    }

}






