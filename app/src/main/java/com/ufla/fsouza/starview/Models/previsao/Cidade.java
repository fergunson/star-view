package com.ufla.fsouza.starview.Models.previsao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fsouza on 13/08/17.
 */

public class Cidade {
    @SerializedName("name")
    private String nomeCidade;

    @SerializedName("country")
    private String pais;

    @SerializedName("population")
    private float population;

    public Cidade(String nomeCidade, String pais, float population) {
        this.nomeCidade = nomeCidade;
        this.pais = pais;
        this.population = population;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public float getPopulation() {
        return population;
    }

    public void setPopulation(float population) {
        this.population = population;
    }
}
