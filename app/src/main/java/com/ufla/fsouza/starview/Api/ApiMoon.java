package com.ufla.fsouza.starview.Api;

/**
 * Created by fsouza on 24/07/17.
 */

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fsouza on 15/07/17.
 * Classe api com os dados da Api da iOasys fornecidas no escopo do projeto
 * endereço do servidor e versão atual da api, essa classe irá criar um objeto Retrofit
 * que irá nos auxiliar a comunicar com este servidor
 */

public class ApiMoon {
    //burning soul offline
    //private static final String ENDPOINT = "http://api.burningsoul.in/moon/";
    private static final String ENDPOINT = "http://api.farmsense.net/v1/";
    public Retrofit getMoonRetrofit(){
        // Trailing slash is needed

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}

