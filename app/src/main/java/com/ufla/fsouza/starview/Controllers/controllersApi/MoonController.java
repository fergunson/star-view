package com.ufla.fsouza.starview.Controllers.controllersApi;

import android.util.Log;

import com.ufla.fsouza.starview.Api.ApiMoon;
import com.ufla.fsouza.starview.Api.ApiServiceMoon;
import com.ufla.fsouza.starview.Controllers.HomeController;
import com.ufla.fsouza.starview.Models.moon.Moon;
import com.ufla.fsouza.starview.Models.moon.MoonList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by fsouza on 02/05/17.
 */

public class MoonController {
    private Retrofit retrofit;
    private final ApiServiceMoon apiService;
    private ArrayList<Moon> condicoesLua;
    private HomeController controle;


    public MoonController(HomeController controle){
        ApiMoon api = new ApiMoon();
        retrofit = api.getMoonRetrofit();
        apiService = retrofit.create(ApiServiceMoon.class);
        this.controle=controle;
    }


    public void proximasCondicoesLua(){
        condicoesLua = new ArrayList<>();
        for (int i=0; i<5;i++){
            long secondsPerDay = 86400*i;
            long unixTimeForRequisition = System.currentTimeMillis()/1000L+secondsPerDay;
            Call call = apiService.getMoon(String.valueOf(unixTimeForRequisition));
            Log.d("URL Moon",call.request().url().toString());
            call.enqueue(new Callback<List<Moon>>() {
                @Override
                public void onResponse(Call<List<Moon>> call, Response<List<Moon>> response) {
                    if (response.isSuccessful()){
                        condicoesLua.add(response.body().get(0));
                        Log.d("Lua ",String.valueOf(response.body()));

                        if(condicoesLua.size()==5){
                            controle.setGotMoonData(1);
                            controle.confereSeChegouDados();
                        }
                    }
                    else {
                        //não foi possível obter resposta
                        Log.e("ERRO", " Lua");
                        controle.setGotMoonData(2);
                        controle.confereSeChegouDados();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Log.d("ERRO", "On Failure Lua");
                    Log.d("ERRO", t.getMessage()+"");
                    controle.setGotMoonData(2);
                    controle.confereSeChegouDados();
                }
            });
        }
    }

    public ArrayList<Moon> getCondicoesLua() {
        return condicoesLua;
    }
}














