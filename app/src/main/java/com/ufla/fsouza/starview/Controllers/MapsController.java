package com.ufla.fsouza.starview.Controllers;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.ufla.fsouza.starview.GoogleMapsFragment;
import com.ufla.fsouza.starview.Models.PontoConsultado;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by fsouza on 21/08/17.
 */

public class MapsController implements Serializable{
    private GoogleMapsFragment googleMapsFragment;
    private LatLng lastLatLong;
    private ArrayList<PontoConsultado> posicoesConsultadas;
    private String nomeLugar;
    private Context context;

    public MapsController (GoogleMapsFragment googleMapsFragment, LatLng lastLatLong, Context context){
        this.googleMapsFragment=googleMapsFragment;
        this.lastLatLong=lastLatLong;
        this.context=context;
        posicoesConsultadas= new ArrayList<>();
    }

    public void novoMarker(LatLng latLng){
        String enderecoDoLugar = pegaEnderecoPelaCoordenada(latLng);
        nomeLugar = pegaNomePelaCoordenada(enderecoDoLugar);
        Log.d("Debug"," "+nomeLugar);
        lastLatLong = latLng;
        PontoConsultado pontoConsultado = new PontoConsultado(
                latLng.latitude,latLng.longitude,enderecoDoLugar);
        posicoesConsultadas.add(pontoConsultado);
        googleMapsFragment.criaMarker(latLng.latitude,latLng.longitude,enderecoDoLugar);
    }

    public LatLng pegaCoordenadaPeloNome(String nome){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> listOfAddress;
        try {
            listOfAddress = geocoder.getFromLocationName(nome,1);
            if(listOfAddress != null && !listOfAddress.isEmpty()){
                Address address = listOfAddress.get(0);
                LatLng latLng = new LatLng(address.getLatitude(),address.getLongitude());
                return latLng;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String pegaNomePelaCoordenada (LatLng latLng){
        return pegaNomePelaCoordenada(pegaEnderecoPelaCoordenada(latLng));
    }
    public String pegaNomePelaCoordenada (String endereco){
        String aux[] = endereco.split(",");
        if (aux.length>=3) {
            return aux[aux.length - 3];
        }
        else {
            return aux[0];
        }
    }

    public String pegaEnderecoPelaCoordenada(LatLng latLng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> listOfAddress;
        try {
            listOfAddress = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (listOfAddress != null && !listOfAddress.isEmpty()) {
                Address address = listOfAddress.get(0);
                return address.getAddressLine(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public LatLng getLastLatLong(){
        return lastLatLong;
    }
    public void setLastLatLong(LatLng lastLatLong) {
        this.lastLatLong = lastLatLong;
    }

    public String getNomeLugar() {
        return nomeLugar;
    }

    public void setNomeLugar(String nomeLugar) {
        this.nomeLugar = nomeLugar;
    }
}
