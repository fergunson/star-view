package com.ufla.fsouza.starview.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ufla.fsouza.starview.R;
import com.ufla.fsouza.starview.Models.CondicaoDia;

import java.util.ArrayList;

/**
 * Created by fsouza on 12/08/17.
 */

public class CondicaoDiaAdapter extends BaseAdapter {
    private ArrayList<CondicaoDia> condicaoDias;
    private LayoutInflater layoutInflater;
    private Context context;

    public CondicaoDiaAdapter(ArrayList<CondicaoDia> condicaoDias, Context context) {
        this.condicaoDias = condicaoDias;
        this.layoutInflater = (LayoutInflater.from(context));
        this.context = context;
    }

    @Override
    public int getCount() {
        return condicaoDias.size();
    }

    @Override
    public Object getItem(int position) {
        return condicaoDias.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.condicao_adapter, null);

        CondicaoDia condicaoDia = condicaoDias.get(position);

        TextView data = (TextView) convertView.findViewById(R.id.dia_horario_adapter);

        data.setText(context.getString(condicaoDia.getForecast().diaDaSemanaParaR()) + " " + condicaoDia.getForecast().getDataCalculada() + " - " + condicaoDia.getForecast().getHora()+"h");

        //mudaBackground(condicaoDia.avalia(), convertView);

        TextView porcentagem = (TextView) convertView.findViewById(R.id.porcentagem_adapter);
        porcentagem.setText(context.getResources().getString(R.string.avaliacao)+String.valueOf(condicaoDia.getPorcentagem()) + "%");

        return convertView;
    }
}