package com.ufla.fsouza.starview;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.ufla.fsouza.starview.GPS.Gps;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by fsouza on 14/05/17.
 */

public class MainActivity extends AppCompatActivity{
    private ImageButton mMapButton, mLocationButton;
    private Gps gps;
    private double latitude, longitude;
    public final static int PERM_REQUEST_CODE_DRAW_OVERLAYS = 1234;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        permissionToDrawOverlays();

        mMapButton =(ImageButton) findViewById(R.id.map_button);
        mLocationButton = (ImageButton) findViewById(R.id.location_button);

        if(criaGPS()){

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }

        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread t = new Thread(){
                    public void run(){
                        startMapActivity();
                    }
                };
                t.start();
            }
        });

        mLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread t = new Thread(){
                    public void run(){
                        startHomeActivity();
                    }
                };
                t.start();
            }
        });
    }

    public void pedeAutorizacaoGPS(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }


    public boolean criaGPS(){
        pedeAutorizacaoGPS();
        gps = new Gps(MainActivity.this);

        if (gps.canGetLocation()) {
            return true;
        }else{
            gps.showSettingsAlert();
            return false;
        }
    }

    public void startHomeActivity() {

        Intent intent = new Intent(MainActivity.this,Home.class);
        intent.putExtra("lat",latitude);
        intent.putExtra("long",longitude);
        intent.putExtra("fragment",1);
        startActivity(intent);
    }

    public void startMapActivity() {
        Intent intent = new Intent(MainActivity.this,Home.class);
        intent.putExtra("fragment",0);
        intent.putExtra("lat",latitude);
        intent.putExtra("long",longitude);

        startActivity(intent);
    }


    /**
     * Permission to draw Overlays/On Other Apps, related to 'android.permission.SYSTEM_ALERT_WINDOW' in Manifest
     * Resolves issue of popup in Android M and above "Screen overlay detected- To change this permission setting you first have to turn off the screen overlay from Settings > Apps"
     * If app has not been granted permission to draw on the screen, create an Intent &
     * set its destination to Settings.ACTION_MANAGE_OVERLAY_PERMISSION &
     * add a URI in the form of "package:<package name>" to send users directly to your app's page.
     * Note: Alternative Ignore URI to send user to the full list of apps.
     */
    public void permissionToDrawOverlays() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, PERM_REQUEST_CODE_DRAW_OVERLAYS);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PERM_REQUEST_CODE_DRAW_OVERLAYS) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {   //Android M Or Over
                if (!Settings.canDrawOverlays(this)) {
                    // ADD UI FOR USER TO KNOW THAT UI for SYSTEM_ALERT_WINDOW permission was not granted earlier...
                }
            }
        }
    }

}








