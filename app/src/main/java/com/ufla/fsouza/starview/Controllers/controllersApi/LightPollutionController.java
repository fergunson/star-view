package com.ufla.fsouza.starview.Controllers.controllersApi;

import android.util.Log;

import com.ufla.fsouza.starview.Api.ApiLightPollution;
import com.ufla.fsouza.starview.Api.ApiServiceLightPollution;
import com.ufla.fsouza.starview.Controllers.HomeController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by fsouza on 24/07/17.
 */

public class LightPollutionController {
    private Retrofit retrofit;
    private final ApiServiceLightPollution apiService;
    private Double radiance;
    static private final String radianceApiKey="vfNqrYqE0G49LJLf";
    private HomeController controle;


    public LightPollutionController(HomeController controle){
        ApiLightPollution api = new ApiLightPollution();
        retrofit = api.getLightPolutionRetrofit();
        apiService = retrofit.create(ApiServiceLightPollution.class);
        this.controle=controle;
    }

    public void obtemRadiacao(String longitude, String latitude){
        String location = longitude+","+latitude;
        Call call = apiService.getRadiance("viirs_2017","point",location,radianceApiKey);
        Log.d("URL Light Pollution",call.request().url().toString());

        call.enqueue(new Callback<Double>() {
            @Override
            public void onResponse(Call<Double> call, Response<Double> response) {
                if (response.isSuccessful()){
                    radiance = response.body();
                    Log.d("Sucesso radiacao", radiance.toString());
                    controle.setGotRadianceData(1);
                    controle.confereSeChegouDados();
                }
                else {
                    Log.e("Erro radiacao", "erro carai");
                    controle.setGotRadianceData(2);
                    controle.confereSeChegouDados();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("ERRO", "On Failure Radiacao");
                Log.d("ERRO", t.getMessage()+"");
                controle.setGotRadianceData(2);
                controle.confereSeChegouDados();
            }


        });
    }

    public Double getRadiance() {
        return radiance;
    }

    public void setRadiance(Double radiance) {
        this.radiance = radiance;
    }
}
