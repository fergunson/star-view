package com.ufla.fsouza.starview.Models.previsao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fsouza on 24/07/17.
 */

public class Wind {
    @SerializedName("speed")
    private float speed;

    @SerializedName("deg")
    private float direction;

    public Wind(float speed, float direction) {
        this.speed = speed;
        this.direction = direction;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Wind speed: "+String.valueOf(speed)+" Dir: "+String.valueOf(direction);
    }
}

