package com.ufla.fsouza.starview.Models.previsao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fsouza on 24/07/17.
 */

public class PrevisaoPrincipal {
    @SerializedName("temp_max")
    private float tempMax;

    @SerializedName("temp_min")
    private float tempMin;

    @SerializedName("pressure")
    private float pressure;

    @SerializedName("humidity")
    private float humidity;

    public PrevisaoPrincipal(float tempMax, float tempMin, float pressure, float humidity) {
        this.tempMax = tempMax;
        this.tempMin = tempMin;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public float getTempMax() {
        return tempMax;
    }

    public void setTempMax(float tempMax) {
        this.tempMax = tempMax;
    }

    public float getTempMin() {
        return tempMin;
    }

    public void setTempMin(float tempMin) {
        this.tempMin = tempMin;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    @Override
    public String toString() {
        return "Temp max: "+String.valueOf(tempMax)+" Min: "+String.valueOf(tempMin);
    }
}


