package com.ufla.fsouza.starview.Models;

import java.util.ArrayList;

/**
 * Created by fsouza on 21/08/17.
 */

public class PontoConsultado {
    private double latitude;
    private double longitude;
    private String nome;
    private ArrayList<CondicaoDia> condicaoDias;

    public PontoConsultado(double latitude, double longitude, String nome) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.nome = nome;
        condicaoDias = new ArrayList<>();
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<CondicaoDia> getCondicaoDias() {
        return condicaoDias;
    }

    public void setCondicaoDias(ArrayList<CondicaoDia> condicaoDias) {
        this.condicaoDias = condicaoDias;
    }
}
