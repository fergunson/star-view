package com.ufla.fsouza.starview.Models.previsao;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import java.util.Calendar;
import com.ufla.fsouza.starview.R;

/**
 * Created by fsouza on 02/05/17.
 */

public class Previsao {

    @SerializedName("dt")
    private long unixTime;

    @SerializedName("main")
    private PrevisaoPrincipal previsaoPrincipal;

    @SerializedName("clouds")
    private Clouds clouds;

    @SerializedName("wind")
    private Wind wind;

    @SerializedName("dt_txt")
    private String date;

    public Previsao(long unixTime, PrevisaoPrincipal previsaoPrincipal, Clouds clouds, Wind wind, String date) {
        this.unixTime = unixTime;
        this.previsaoPrincipal = previsaoPrincipal;
        this.clouds = clouds;
        this.wind = wind;
        this.date=date;
    }


    public PrevisaoPrincipal getPrevisaoPrincipal() {
        return previsaoPrincipal;
    }

    public void setPrevisaoPrincipal(PrevisaoPrincipal previsaoPrincipal) {
        this.previsaoPrincipal = previsaoPrincipal;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Previsao "+previsaoPrincipal.toString()+" "+wind.toString()+" "+clouds.toString();
    }

    public int diaDaSemanaParaR (){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixTime*1000L);
        int indexDia = calendar.get(Calendar.DAY_OF_WEEK);
        Log.d("Id dia da semana",String.valueOf(indexDia));
        if (indexDia==1){
            return R.string.domingo;
        }
        else if (indexDia==2){
            return R.string.segunda;
        }
        else if (indexDia==3){
            return R.string.terca;
        }
        else if (indexDia==4){
            return R.string.quarta;
        }
        else if (indexDia==5){
            return R.string.quinta;
        }
        else if (indexDia==6){
            return R.string.sexta;
        }
        return R.string.sabado;
    }

    public String getDataCalculada (){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixTime*1000);
        int diaDoMes = Integer.valueOf(calendar.get(Calendar.DATE));
        int mes = Integer.valueOf(calendar.get(Calendar.MONTH))+1;
        return String.valueOf(diaDoMes+"/"+mes);
    }

    public int getHora (){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(unixTime*1000);
        return Integer.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
    }
}
