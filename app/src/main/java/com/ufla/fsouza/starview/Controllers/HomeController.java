package com.ufla.fsouza.starview.Controllers;
import android.util.Log;

import com.ufla.fsouza.starview.Controllers.controllersApi.LightPollutionController;
import com.ufla.fsouza.starview.Controllers.controllersApi.MoonController;
import com.ufla.fsouza.starview.Controllers.controllersApi.WeatherController;
import com.ufla.fsouza.starview.HomeFragment;
import com.ufla.fsouza.starview.Models.CondicaoDia;
import com.ufla.fsouza.starview.Models.moon.Moon;
import com.ufla.fsouza.starview.Models.moon.MoonList;
import com.ufla.fsouza.starview.Models.previsao.Previsao;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by fsouza on 27/04/17.
 */

    public class HomeController {

    private WeatherController weatherController;
    private MoonController moonController;
    private LightPollutionController lightPollutionController;

    private int gotWeatherData; //0-nada 1-sucesso 2-erro
    private int gotMoonData;    //0-nada 1-sucesso 2-erro
    private int gotRadianceData;  //0-nada 1-sucesso 2-erro
    private HomeFragment homeFragment;
    private LatLng latLng;
    private String lugarAtual;

    public HomeController(HomeFragment homeFragment,LatLng latLng, String lugarAtual){
        this.homeFragment = homeFragment;
        this.latLng=latLng;
        this.lugarAtual=lugarAtual;

        weatherController = new WeatherController(this);
        moonController = new MoonController(this);
        lightPollutionController = new LightPollutionController(this);
    }

    public void atualizaDados(){
        homeFragment.iniciaCarregamento();
        moonController.proximasCondicoesLua();
        weatherController.refreshForecast(
                String.valueOf(latLng.longitude),
                String.valueOf(latLng.latitude),
                "metric",
                "pt");
        lightPollutionController.obtemRadiacao(
                String.valueOf(latLng.longitude),
                String.valueOf(latLng.latitude));
    }

    public void pegaCondicoes(){
            ArrayList<CondicaoDia> condicoesDias = new ArrayList<>();
            ArrayList<Moon> condicoesLua= moonController.getCondicoesLua();

            Log.d("Quantidade de previsões", String.valueOf(weatherController.getPrevisoes().size()));

            ArrayList<Previsao> previsoes = weatherController.getPrevisoes();
            if (previsoes.size()!=0){
                //ArrayList<Previsao> previsoes = filtraPorHorario(weatherController.getPrevisoes());

                int i=0;
                for (Previsao p :previsoes ) {

                    if (p.getHora()<6 || p.getHora()>18) {

                        CondicaoDia condicaoDia = new CondicaoDia(
                                condicoesLua.get(i / 8),
                                lightPollutionController.getRadiance(),
                                p, weatherController.getCidade());
                        Log.d("condicoesDias", String.valueOf(i));
                        i++;
                        condicoesDias.add(condicaoDia);
                    }
                }
                try {
                    homeFragment.escreveDados(condicoesDias, getMelhorCondicao(condicoesDias), lugarAtual);
                    zeraStatus();
                }catch (SecurityException e) {
                    Log.d("Error: ","Não conseguiu escrever dados");
                }
            }
    }

    /*
    public ArrayList<Previsao> filtraPorHorario(ArrayList<Previsao> previsoes){
        ArrayList<Previsao> previsoesFiltradas = new ArrayList<>();
        for (Previsao p: previsoes) {

            //int c = p.getCalendar().HOUR_OF_DAY;
            Log.d("Hora ", String.valueOf(c));
            if((c>18)||(c<6)){
                Log.d("adicionou ", String.valueOf(p));
                previsoesFiltradas.add(p);
            }
        }
        return previsoesFiltradas;
    }
*/


    public void confereSeChegouDados(){
        if((gotMoonData==1)&&(gotRadianceData==1)&&(gotWeatherData==1)){
            pegaCondicoes();
        }
        else if ((gotMoonData==2)||(gotRadianceData==2)||(gotWeatherData==2)){
            homeFragment.erroConexao();
        }
    }

    public void setGotWeatherData(int gotWeatherData) {
        this.gotWeatherData = gotWeatherData;
    }

    public void setGotMoonData(int gotMoonData) {
        this.gotMoonData = gotMoonData;
    }

    public void setGotRadianceData(int gotRadianceData) {
        this.gotRadianceData = gotRadianceData;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public void zeraStatus(){
        setGotMoonData(0);
        setGotRadianceData(0);
        setGotWeatherData(0);
    }

    private CondicaoDia getMelhorCondicao(ArrayList<CondicaoDia> condicaoDias){
        CondicaoDia condicaoDia = condicaoDias.get(0);
        for (CondicaoDia c: condicaoDias) {
            if (condicaoDia.getPorcentagem()<c.getPorcentagem()){
                condicaoDia = c;
            }
        }
        return condicaoDia;
    }

    public String getLugarAtual() {
        return lugarAtual;
    }

    public void setLugarAtual(String lugarAtual) {
        this.lugarAtual = lugarAtual;
    }
}
