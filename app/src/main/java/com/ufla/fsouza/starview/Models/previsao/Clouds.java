package com.ufla.fsouza.starview.Models.previsao;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fsouza on 24/07/17.
 */

public class Clouds {
    @SerializedName("all")
    private float cloudsPercent;

    public Clouds(float cloudsPercent) {
        this.cloudsPercent = cloudsPercent;
    }

    public float getCloudsPercent() {
        return cloudsPercent;
    }

    public void setCloudsPercent(float cloudsPercent) {
        this.cloudsPercent = cloudsPercent;
    }

    @Override
    public String toString() {
        return "Clouds: "+String.valueOf(cloudsPercent);
    }
}
