package com.ufla.fsouza.starview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ufla.fsouza.starview.Controllers.MasterController;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;


public class GoogleMapsFragment extends Fragment implements OnMapReadyCallback, Serializable {
    final public String nomeFragmento = "Map";
    private EditText mEditBusca;
    private TextView mPlaceName;
    private Button mSubmitButton;
    private GoogleMap mMap;
    private LatLng lastLatLng;
    private View viewMap = null;
    private SupportMapFragment mapFragment=null;

    private MasterController masterController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lastLatLng = new LatLng(masterController.getMapsController().getLastLatLong().latitude
                ,masterController.getMapsController().getLastLatLong().longitude);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(savedInstanceState!=null){
            LatLng latLng = new LatLng(savedInstanceState.getDouble("lat"),
                    savedInstanceState.getDouble("long"));
            lastLatLng = latLng;
        }

        Log.d("On create View ","true");

        try{
            viewMap = inflater.inflate(R.layout.fragment_map, container, false);
        }catch (Exception e){
            Log.e("ERRO", "onCreateView", e);
            throw e;
        }

        mEditBusca = (EditText) viewMap.findViewById(R.id.edit_text_buscaLocalizacao);
        mSubmitButton = (Button) viewMap.findViewById(R.id.submit_button);
        mPlaceName = (TextView) viewMap.findViewById(R.id.place_name);

        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapa);
        mapFragment.getMapAsync(this);


        mEditBusca.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                LatLng latLng = masterController.getMapsController().pegaCoordenadaPeloNome(mEditBusca.getText().toString());
                if (latLng!=null){
                    masterController.getMapsController().novoMarker(latLng);
                    lastLatLng = latLng;
                }
                return false;
            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lastLatLng!=null){
                    Log.d("Passa prox pagina","passa pagina");
                    masterController.getHome().setPage(1);
                    masterController.getHomeController().setLatLng(lastLatLng);
                    masterController.getHomeController().atualizaDados();
                    masterController.getHomeController().setLugarAtual(masterController.getMapsController().getNomeLugar());

                }
            }
        });
        return viewMap;
    }

    public void criaMarker(double lat, double lon, String name){
        mPlaceName.setText(name);
        LatLng latLng = new LatLng(lat,lon);
        lastLatLng=latLng;
        mMap.addMarker(new MarkerOptions().position(latLng).title(name));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(getContext(),getString(R.string.toast_map),Toast.LENGTH_LONG).show();
        mMap = googleMap;
        masterController.getMapsController().novoMarker(lastLatLng);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                masterController.getMapsController().novoMarker(latLng);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                lastLatLng=new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);
                return false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        masterController.getHome().getHomeFragment().setMasterController(masterController);
        masterController.getHome().getAboutFragment().setMasterController(masterController);
        if (mapFragment!=null){
            getFragmentManager().beginTransaction().remove(mapFragment).commitAllowingStateLoss();
            getFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
        }
        Log.d("Destruiu view map ", "destrui");
    }

    /**
     * @Override
    public void onDestroyView() {
    super.onDestroyView();
    MapFragment f = (MapFragment) getFragmentManager()
    .findFragmentById(R.id.mymap);
    if (f != null)
    getFragmentManager().beginTransaction().remove(f).commit();
    }
     */
    @Override
    public void onDestroy() {
        masterController.getHome().getHomeFragment().setMasterController(masterController);
        masterController.getHome().getAboutFragment().setMasterController(masterController);
        super.onDestroy();
        masterController.getHome().getSupportFragmentManager().beginTransaction().remove(this);
        Log.d("Fragment", "Destrui Maps");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble("lastLat",lastLatLng.latitude);
        outState.putDouble("lastLong",lastLatLng.longitude);

    }

    @Override
    public String toString() {
        return nomeFragmento;
    }

    public MasterController getMasterController() {
        return masterController;
    }

    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }

    public void setmPlaceName(String mPlaceName) {
        this.mPlaceName.setText(mPlaceName);
    }
}
