package com.ufla.fsouza.starview.Api;

/**
 * Created by fsouza on 24/07/17.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fsouza on 24/07/17.
 */

public class ApiWeather {
    private static final String ENDPOINT = "https://api.openweathermap.org/";
    private static final String VERSION = "2.5";

    public Retrofit getWeatherRetrofit(){
        // Trailing slash is needed
        //"dt_txt":"2014-07-23 09:00:00"}
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd''HH:mm:ss")//"dt_txt":"2014-07-23 09:00:00"}
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT+"/data/"+VERSION+"/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }
}


