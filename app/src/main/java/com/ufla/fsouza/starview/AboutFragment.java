package com.ufla.fsouza.starview;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ufla.fsouza.starview.Controllers.MasterController;

import java.io.Serializable;



public class AboutFragment extends Fragment implements Serializable{
    final public String  nomeFragmento = "About";
    private MasterController masterController;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false);
    }


    @Override
    public void onDestroyView() {
        masterController.getHome().getHomeFragment().setMasterController(masterController);
        masterController.getHome().getGoogleMapsFragment().setMasterController(masterController);
        super.onDestroyView();
        Log.d("Destruiu view about ", "destrui");
    }

    @Override
    public void onDestroy() {
        masterController.getHome().getHomeFragment().setMasterController(masterController);
        masterController.getHome().getGoogleMapsFragment().setMasterController(masterController);
        super.onDestroy();
        Log.d("Fragment", "Destrui About");
    }

    @Override
    public String toString() {
        return nomeFragmento;
    }

    public MasterController getMasterController() {
        return masterController;
    }

    public void setMasterController(MasterController masterController) {
        this.masterController = masterController;
    }

}
