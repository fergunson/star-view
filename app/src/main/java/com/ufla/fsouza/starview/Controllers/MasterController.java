package com.ufla.fsouza.starview.Controllers;

import com.ufla.fsouza.starview.Home;

import java.io.Serializable;

/**
 * Created by fsouza on 22/08/17.
 */

public class MasterController implements Serializable{
    private HomeController homeController;
    private MapsController mapsController;
    private Home home;
    private int firstFocus;

    public MasterController(HomeController homeController, MapsController mapsController, Home home, int firstFocus) {
        this.homeController = homeController;
        this.mapsController = mapsController;
        this.home=home;
        this.firstFocus=firstFocus;
        home.setPage(firstFocus);
    }

    public HomeController getHomeController() {
        return homeController;
    }

    public MapsController getMapsController() {
        return mapsController;
    }

    public Home getHome() {
        return home;
    }

    public int getFirstFocus() {
        return firstFocus;
    }
}
