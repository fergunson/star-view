package com.ufla.fsouza.starview.Api;

import com.ufla.fsouza.starview.Models.moon.Moon;
import com.ufla.fsouza.starview.Models.moon.MoonList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by fsouza on 24/07/17.
 */

public interface ApiServiceMoon {

    @GET("moonphases/")
    Call<List<Moon>> getMoon(
            @Query("d") String unixTime
    );
}
