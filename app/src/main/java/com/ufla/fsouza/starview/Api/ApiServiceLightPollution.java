package com.ufla.fsouza.starview.Api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fsouza on 24/07/17.
 */

public interface ApiServiceLightPollution {

    String radianceApiKey="vfNqrYqE0G49LJLf";
    //https://www.lightpollutionmap.info/

    @GET("QueryRaster/")
    //https://www.lightpollutionmap.info/QueryRaster/?ql=viirs_2016&qt=point&qd=14.706640000368957,46.05480112571584&key=vfNqrYqE0G49LJLf
    Call<Double> getRadiance(
            @Query("ql") String dataVersion,
            @Query("qt") String type,
            @Query("qd") String location,
            @Query("key") String key);
}
