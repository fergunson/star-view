package com.ufla.fsouza.starview.Api;

import com.ufla.fsouza.starview.Models.previsao.WeatherList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fsouza on 24/07/17.
 */

public interface ApiServiceWeather {
    //http://api.openweathermap.org/

    /*
    @GET("/users?filters[0][field]={param}&filters[0][operator]=equals&filters[0][value]={value}")
    @GET("/users?filters[0][operator]=equals")

       UserDto retrieveUsersByFilters(
            @Path("param") String nameFilter,
            @Path("value") String value);

        @GET("/users?filters[0][operator]=equals")
          UserDto retrieveUsersByFilters(
          @Query("filters[0][field]") String nameFilter,
          @Query("filters[0][value]") String value);
     */
    @GET("forecast")
    Call<WeatherList> getWeather(
            @Query("lon") String lon,
            @Query("lat") String lat,
            @Query("units") String units,
            @Query("lang") String lang,
            @Query("appid") String weatherApiKey);
}
