package com.ufla.fsouza.starview.Models;

import android.util.Log;

import com.ufla.fsouza.starview.Models.moon.Moon;
import com.ufla.fsouza.starview.R;
import com.ufla.fsouza.starview.Models.previsao.Cidade;
import com.ufla.fsouza.starview.Models.previsao.Previsao;

/**
 * Created by fsouza on 06/05/17.
 */

public class CondicaoDia {
    private Double moonIllumination;
    private Double radiance;
    private Previsao forecast;
    private int porcentagem;
    private Cidade cidade;

    public CondicaoDia(Moon moon, double radiance, Previsao previsao, Cidade cidade) {
        this.moonIllumination = moon.getIllumination();
        this.radiance = radiance;
        this.forecast = previsao;
        this.cidade=cidade;

        avaliacao();
    }

    public void avaliacao(){

        double clouds = forecast.getClouds().getCloudsPercent()/100;

        double exp =Math.exp(-0.1442628*radiance);
        double sqm = 19.34557 + 2.352514*exp;

        double lightPollution = sqm+0.36*(moonIllumination/100);

        double lightPollutionPercent = 778.5698 - 35.7142*lightPollution;

        radiance = 778.5698 - 35.7142*sqm;

        if (lightPollutionPercent>100){
            lightPollutionPercent=100;
        }
        if (lightPollutionPercent<0){
            lightPollutionPercent=0;
        }
        /*
        double humidity = forecast.getPrevisaoPrincipal().getHumidity();

        Log.d("Radiação pre calculo: ", String.valueOf(radiance));
        double diference=0;
        if (radiance>20){
            diference = radiance-20;
        }

        Log.d("Diferença: ", String.valueOf(diference));

        radiance= (radiance*100+((diference*0.7)/radiance))/40;

        if (radiance>100){
            radiance=100.0;
        }

        double finalValue;

        if(clouds>70 &&radiance<80){
            finalValue = clouds*0.6+ radiance*0.3+ moonIllumination*0.05+humidity*0.05;
        }
        else if (radiance>80 && clouds<70){
            finalValue = clouds * 0.3 + radiance * 0.65+humidity*0.01+moonIllumination*0.04;
        }
        else{
            finalValue =(moonIllumination*0.1+radiance*0.6+clouds*0.35+humidity*0.02);
        }
        */

        Double d = (1 - clouds)*(100-lightPollutionPercent);
        porcentagem = d.intValue();
        if (porcentagem<0){
            porcentagem=0;
        }


        Log.d("Calculo condicoes", "SQM: "+String.valueOf(sqm)+
                " Light pollution percent:"+String.valueOf(lightPollutionPercent)+" clouds:"+String.valueOf((1-clouds)+
                " total: "+String.valueOf(porcentagem)));

    }

    public int avalia(){

        if(porcentagem>=80){
            return R.string.quality_excellent;
        }
        else if((porcentagem>=60) && (porcentagem<80)){
            return R.string.quality_good;
        }
        else if((porcentagem>=40) && (porcentagem<60)){
            return R.string.quality_medium;
        }
        else{
            return R.string.quality_low;
        }
    }

    public int getMoonIllumination() {
        return moonIllumination.intValue();
    }

    public int getRadiance() {
        return radiance.intValue();
    }

    public Previsao getForecast() {
        return forecast;
    }

    public int getPorcentagem() {
        return porcentagem;
    }

    public String toString(){
        return forecast.getDate().substring(0,16)+"   "+ String.valueOf(porcentagem).substring(0,3)+"% ";
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

}
