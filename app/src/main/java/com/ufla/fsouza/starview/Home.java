package com.ufla.fsouza.starview;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.ufla.fsouza.starview.Adapters.ViewPagerAdapter;
import com.ufla.fsouza.starview.Controllers.HomeController;
import com.ufla.fsouza.starview.Controllers.MapsController;
import com.ufla.fsouza.starview.Controllers.MasterController;
import com.google.android.gms.maps.model.LatLng;
public class Home extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private double latitude, longitude;
    private int fragmentFocus;
    private GoogleMapsFragment googleMapsFragment = new GoogleMapsFragment();
    private HomeFragment homeFragment = new HomeFragment();
    private AboutFragment aboutFragment = new AboutFragment();
    private MasterController masterController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Intent intent = getIntent();
        latitude = intent.getDoubleExtra("lat",0);
        longitude = intent.getDoubleExtra("long",0);
        fragmentFocus = intent.getIntExtra("fragment",0);

        LatLng latLng = new LatLng(latitude,longitude);
        MapsController mapsController = new MapsController(googleMapsFragment,latLng,getBaseContext());
        HomeController homeController = new HomeController(homeFragment,latLng,mapsController.pegaNomePelaCoordenada(latLng));
        masterController= new MasterController(homeController,mapsController,this,fragmentFocus);

        googleMapsFragment.setMasterController(masterController);
        homeFragment.setMasterController(masterController);
        aboutFragment.setMasterController(masterController);

        setupViewPager(viewPager);
        setPage(fragmentFocus);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(googleMapsFragment,getString(R.string.title_activity_maps));
        adapter.addFragment(homeFragment,getString(R.string.title_activity_home));
        adapter.addFragment(aboutFragment,getString(R.string.title_activity_about));
        viewPager.setAdapter(adapter);
    }

    public void setPage(int position){
        viewPager.setCurrentItem(position);
    }

    public MasterController getMasterController() {
        return masterController;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        getSupportFragmentManager().beginTransaction().remove(googleMapsFragment).commitAllowingStateLoss();
        getSupportFragmentManager().beginTransaction().remove(homeFragment).commitAllowingStateLoss();
        getSupportFragmentManager().beginTransaction().remove(aboutFragment).commitAllowingStateLoss();
    }

    public GoogleMapsFragment getGoogleMapsFragment() {
        return googleMapsFragment;
    }

    public void setGoogleMapsFragment(GoogleMapsFragment googleMapsFragment) {
        this.googleMapsFragment = googleMapsFragment;
    }

    public HomeFragment getHomeFragment() {
        return homeFragment;
    }

    public void setHomeFragment(HomeFragment homeFragment) {
        this.homeFragment = homeFragment;
    }

    public AboutFragment getAboutFragment() {
        return aboutFragment;
    }

    public void setAboutFragment(AboutFragment aboutFragment) {
        this.aboutFragment = aboutFragment;
    }
}