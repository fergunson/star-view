package com.ufla.fsouza.starview.Models.previsao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by fsouza on 02/05/17.
 */

public class WeatherList {
    @SerializedName("message")
    String message;

    @SerializedName("list")
    private ArrayList<Previsao> previsoes;

    @SerializedName("city")
    private Cidade cidade;

    public WeatherList(String message, ArrayList<Previsao> previsoes, Cidade cidade) {
        this.message = message;
        this.previsoes = previsoes;
        this.cidade = cidade;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Previsao> getPrevisoes() {
        return previsoes;
    }

    public void setPrevisoes(ArrayList<Previsao> previsoes) {
        this.previsoes = previsoes;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
}








